// USAGE
// facebookScrollTo(100);

function facebookScrollTo(y){
	FB.Canvas.getPageInfo(function(pageInfo){
		$({y: pageInfo.scrollTop}).animate(
			{y: y},
			{duration: 1000, step: function(offset){
				FB.Canvas.scrollTo(0, offset);
			}
		});
	});
}